package aesprotector;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Scanner;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class AESProtector {

    /**
     * String to hold name of the encryption algorithm.
     */
    public static final String ALGORITHM = "RSA";

    /**
     * String to hold the name of the private key file.
     */
    public static final String PRIVATE_KEY_FILE = "C:/keys/private.key";

    /**
     * String to hold name of the public key file.
     */
    public static final String PUBLIC_KEY_FILE = "C:/keys/public.key";

    public static final String SIMETRIC_KEY = "C:/keys/simetric.txt";

    public static void wipe(File f) throws Exception {
        try (RandomAccessFile rand = new RandomAccessFile(f, "rw")) {
            for (int i = 0; i < rand.length(); i++) {
                rand.write(0);
            }
        }
    }

    // Encripta la clau i no les dades.
    public static void encrypt(File file, PublicKey key) {
        //byte[] cipherText = null;
        String finalName = file.getAbsolutePath()+".aes";
        try {
            // get an RSA cipher object and print the provider
            final Cipher cipher = Cipher.getInstance(ALGORITHM);
            // encrypt the plain text using the public key
            cipher.init(Cipher.ENCRYPT_MODE, key);
            //cipherText = cipher.doFinal(text.getBytes());
            byte[] data = new byte[1024];
            byte[] res;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try (FileInputStream in = new FileInputStream(file)) {
                int read = in.read(data);
                while (read > 0) {
                    res = cipher.update(data, 0, read);
                    out.write(res);
                    read = in.read(data);
                }
            }
            res = cipher.doFinal();
            out.write(res);
            out.close();
            try (FileOutputStream outFile = new FileOutputStream(finalName)) {
                outFile.write(res);
            }
            //S'elimina totalment el fitxer antic
            //AESProtector.wipe(file);
            //file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Desencripta la clau
    //Retorna clau simetrica
    public static byte[] decrypt(File file, PrivateKey key) {
        //byte[] dectyptedText = null;
        String finalName = file.getAbsolutePath();
        try {
            // get an RSA cipher object and print the provider
            final Cipher cipher = Cipher.getInstance(ALGORITHM);
            // decrypt the text using the private key
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] data = new byte[1024];
            byte[] res;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try (FileInputStream in = new FileInputStream(file)) {
                int read = in.read(data);
                while (read > 0) {
                    res = cipher.update(data, 0, read);
                    out.write(res);
                    read = in.read(data);
                }
            }
            res = cipher.doFinal();
            out.write(res);
            out.close();
            try (FileOutputStream outFile = new FileOutputStream(finalName.substring(0, (int)finalName.length() - 4))) {
                outFile.write(res);
            }
            //S'elimina totalment el fitxer antic
//            AESProtector.wipe(file);
//            file.delete();
            return res;
        } catch (Exception ex) {
            ex.printStackTrace();
            return new byte[0];
        }
    }

    public static void generateKey() {
        try {
            final KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITHM);
            keyGen.initialize(1024);
            final KeyPair key = keyGen.generateKeyPair();

            File privateKeyFile = new File(PRIVATE_KEY_FILE);
            File publicKeyFile = new File(PUBLIC_KEY_FILE);

            // Create files to store public and private key
            if (privateKeyFile.getParentFile() != null) {
                privateKeyFile.getParentFile().mkdirs();
            }
            privateKeyFile.createNewFile();
            if (publicKeyFile.getParentFile() != null) {
                publicKeyFile.getParentFile().mkdirs();
            }
            publicKeyFile.createNewFile();

            // Saving the Public key in a file
            ObjectOutputStream publicKeyOS = new ObjectOutputStream(
                    new FileOutputStream(publicKeyFile));
            publicKeyOS.writeObject(key.getPublic());
            publicKeyOS.close();

            // Saving the Private key in a file
            ObjectOutputStream privateKeyOS = new ObjectOutputStream(
                    new FileOutputStream(privateKeyFile));
            privateKeyOS.writeObject(key.getPrivate());
            privateKeyOS.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static SecretKey passwordKeyGeneration(String text) throws Exception {
        byte[] data = text.getBytes("UTF-8");
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] hash = md.digest(data);
        byte[] key = Arrays.copyOf(hash, 196 / 8);
        return new SecretKeySpec(key, "AES");
    }

    public static boolean areKeysPresent() {

        File privateKey = new File(PRIVATE_KEY_FILE);
        File publicKey = new File(PUBLIC_KEY_FILE);

        if (privateKey.exists() && publicKey.exists()) {
            return true;
        }

        return false;
    }

    /**
     * Un programa que protegeix i desprotegeix un fitxer amb contrasenya
     */
    public static void main(String[] args) throws Exception {
        Scanner scan = new Scanner(System.in);
        System.out.println("Encriptar - 0 ||| Desencriptar - 1 || Generar Claus - 2");
        String opcio = scan.nextLine();
        System.out.print("Tria el fitxer: ");
        String f = scan.nextLine();
        File file = new File(f);
        File clau = new File(SIMETRIC_KEY);
        if (file.isFile()) {
            try {
                if (opcio.equals("0")) {//Emisor
                    String pass = new Scanner(clau).nextLine();                 //llegir clau
                    SecretKey sKey = AESProtector.passwordKeyGeneration(pass);
                    Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                    cipher.init(Cipher.ENCRYPT_MODE, sKey);                     //xifra dades amb la clau simetrica
                    //Es llegeixen les dades i es van acumulant successivament xifrades
                    byte[] data = new byte[1024];
                    byte[] res;
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    try (FileInputStream in = new FileInputStream(file)) {
                        int read = in.read(data);
                        while (read > 0) {
                            res = cipher.update(data, 0, read);
                            out.write(res);
                            read = in.read(data);
                        }
                    }
                    res = cipher.doFinal();
                    out.write(res);
                    out.close();

                    //Escrivim les dades al nou fitxer
                    res = out.toByteArray();
                    try (FileOutputStream outFile = new FileOutputStream(file.getAbsolutePath()+".aes")) {
                        outFile.write(res);
                    }
                    // Encrypt la clau simetrica
                    ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(PUBLIC_KEY_FILE));
                    final PublicKey publicKey = (PublicKey) inputStream.readObject();
                    encrypt(clau, publicKey);                    
                }
                if (opcio.equals("1")) {//receptor
                    System.out.println("Escriu la path de la contrasenya:");
                    File clauXifrada = new File(scan.nextLine());
                    ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(PRIVATE_KEY_FILE));// desencriptar clau
                    final PrivateKey privateKey = (PrivateKey) inputStream.readObject();
                    
                    MessageDigest md = MessageDigest.getInstance("SHA-256");
                    byte[] hash = md.digest(decrypt(clauXifrada, privateKey));//                                                           //desencriptar clau
                    byte[] key = Arrays.copyOf(hash, 196 / 8);
                    SecretKey sKey = new SecretKeySpec(key, "AES");                                              // desencripto dades
                    String finalName = file.getAbsolutePath().substring(0, (int)file.getAbsolutePath().length() - 4);
                    Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                    cipher.init(Cipher.DECRYPT_MODE, sKey);
                    //Es llegeixen les dades i es van acumulant successivament xifrades
                    byte[] data = new byte[1024];
                    byte[] res;
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    try (FileInputStream in = new FileInputStream(file)) {
                        int read = in.read(data);
                        while (read > 0) {
                            res = cipher.update(data, 0, read);
                            out.write(res);
                            read = in.read(data);
                        }
                    }
                    res = cipher.doFinal();
                    out.write(res);
                    out.close();

                    //Escrivim les dades al nou fitxer
                    res = out.toByteArray();
                    try (FileOutputStream outFile = new FileOutputStream(file.getAbsolutePath().substring(0, (int)file.getAbsolutePath().length() - 4))) {
                        outFile.write(res);
                    }
                }
                if (opcio.equals("2")) {//receptor
                    generateKey();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("No existeix aquest fitxer.");
        }
    }
}
